#move config file
mv /var/www/html/.env /var/www/.env
#move vendor
mv /var/www/html/vendor/ /var/www/vendor/
#empty directory
rm -rf /var/www/html/*
mv /var/www/build.tar.gz /var/www/html/build.tar.gz
#untar build
cd /var/www/html/
tar xvf build.tar.gz
#move config files
mv /var/www/.env /var/www/html/.env
#move vendor
mv /var/www/vendor/ /var/www/html/vendor/
#permissions for var
mkdir /var/www/html/var/
chmod -R 777 /var/www/html/var/
