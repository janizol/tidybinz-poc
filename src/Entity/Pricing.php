<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PricingRepository")
 */
class Pricing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100000, nullable=true)
     */
    private $rules_text;

    /**
     * @ORM\Column(type="integer")
     */
    private $period_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRulesText(): ?string
    {
        return $this->rules_text;
    }

    public function setRulesText(?string $rules_text): self
    {
        $this->rules_text = $rules_text;

        return $this;
    }

    public function getPeriodId(): ?int
    {
        return $this->period_id;
    }

    public function setPeriodId(int $period_id): self
    {
        $this->period_id = $period_id;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }
}
